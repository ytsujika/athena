/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FPGATRACKSIMTRUTHTRACKCOLLECTION_H
#define FPGATRACKSIMTRUTHTRACKCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "AthenaKernel/CLASS_DEF.h"
#include "FPGATrackSimObjects/FPGATrackSimTruthTrack.h"

// The magic number here should be generated by "clid -m classname" according to
// https://twiki.cern.ch/twiki/bin/view/Main/AthenaCodeSnippets#Registering_user_defined_classes
typedef std::vector<FPGATrackSimTruthTrack> FPGATrackSimTruthTrackCollection;
CLASS_DEF( FPGATrackSimTruthTrackCollection , 40073434 , 1 )

#endif // FPGATRACKSIMTRUTHTRACKCOLLECTION_DEF_H
